class Render_Helper {
  constructor() {
    this.target = "";
    this.target_data = "";
  }

  set_target_el(target_el) {
    this.target = target_el;
  }
  get_target_el() {
    return this.target;
  }

  set_target_data(data) {
    this.target_data = data;
  }

  get_target_data() {
    return this.target_data;
  }
  clear_content() {
    document.querySelectorAll(this.get_target_el())[0].innerHTML = "";
  }
  create_html_template() {
    let html_template = "";
    let data = this.get_target_data();
    data.forEach((student) => {
      html_template += `
          <tr class="student student-${student._id}">
              <td>${student._id}</td>
              <td>${student._name}</td>
              <td>${student._email}</td>
              <td>${student.getAvg()}</td>
              <td class="action">
                  <i class="icon icon-edit fa-solid fa-pen-to-square" 
                  data-target="${student._id}"></i>
                  <i class="icon icon-del fa-solid fa-xmark" 
                  data-target="${student._id}"></i>
              </td>
          </tr>
      `;
    });
    return html_template;
  }
  bind_el_event(_target, event, callback) {
    if (_target instanceof HTMLElement) {
      _target.addEventListener(event, callback);
      return;
    } else {
      document.querySelector(_target).addEventListener(event, callback);
    }
  }
  render_html({ edit_callback, delete_callback }) {
    this.clear_content();
    document.querySelectorAll(this.get_target_el())[0].innerHTML =
      this.create_html_template();
    document.querySelectorAll(".icon-edit").forEach((edit_btn) => {
      this.bind_el_event(edit_btn, "click", () => {
        edit_callback(edit_btn.dataset.target);
      });
    });
    document.querySelectorAll(".icon-del").forEach((del_btn) => {
      this.bind_el_event(del_btn, "click", () => {
        delete_callback(del_btn.dataset.target);
      });
    });
  }
}

export { Render_Helper };
