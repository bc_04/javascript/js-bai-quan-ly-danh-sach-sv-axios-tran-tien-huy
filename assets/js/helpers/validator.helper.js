class Validator_Helper {
  constructor(_value, _target) {
    this._value = _value;
    this._target = _target;
    this._message = "";
  }
  set_err_msg(msg) {
    this._message = msg;
  }

  get_err_msg() {
    return this._message;
  }

  is_empty(msg) {
    this.set_err_msg(msg);
    if (this._value.length) {
      this.set_err_msg("");
    }
    document.querySelectorAll(this._target)[0].innerText = this.get_err_msg();
    return this._value.length > 0;
  }

  is_exist(msg, data_list = [], key) {
    this.set_err_msg(msg);

    let find_index = data_list.findIndex(
      (student) => student[key] === this._value
    );
    if (find_index == -1) {
      this.set_err_msg("");
    }
    document.querySelectorAll(this._target)[0].innerText = this.get_err_msg();
    return find_index;
  }

  is_smaller(msg, min) {
    this.set_err_msg(msg);
    this._value.length >= min && this.set_err_msg("");
    document.querySelectorAll(this._target)[0].innerText = this.get_err_msg();
    return this._value.length >= min;
  }

  is_greater(msg, max) {
    this.set_err_msg(msg);
    this._value.length <= max && this.set_err_msg("");
    document.querySelectorAll(this._target)[0].innerText = this.get_err_msg();
    return this._value.length <= max;
  }

  is_valid_email(msg) {
    const REGEX_EMAIL =
      /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    this.set_err_msg(msg);
    REGEX_EMAIL.test(this._value) && this.set_err_msg("");
    document.querySelectorAll(this._target)[0].innerText = this.get_err_msg();
    return REGEX_EMAIL.test(this._value);
  }
}

export { Validator_Helper };
