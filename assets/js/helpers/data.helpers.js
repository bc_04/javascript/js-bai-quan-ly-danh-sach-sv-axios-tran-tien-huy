let save_local_storage = (name, data) => {
  localStorage.setItem(name, JSON.stringify(data));
};

let load_local_storage = (name) => {
  return JSON.parse(localStorage.getItem(name)) || [];
};

let del_local_storage = (name) => {
  localStorage.removeItem(name);
};

export { save_local_storage, load_local_storage, del_local_storage };
