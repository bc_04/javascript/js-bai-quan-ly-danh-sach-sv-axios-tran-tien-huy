class Form_Helper {
  constructor() {
    this.field_list = [];
  }
  register_fields(fields) {
    this.field_list = [...fields];
  }
  get_form_data() {
    let field_data = {};
    this.field_list.forEach((field) => {
      field_data[document.querySelector(field).dataset.name] = document
        .querySelector(field)
        .value.trim();
    });
    return field_data;
  }

  set_form_data(data_list = []) {
    if (data_list.length > 0 && data_list.length != this.field_list.length) {
      console.log(
        "Please input a correct data list based on the number of fields"
      );
      return -1;
    }
    if (data_list.length == 0) {
      this.field_list.forEach((field) => {
        document.querySelector(field).value = "";
      });

      return 1;
    }
    this.field_list.forEach((field, index) => {
      document.querySelector(field).value = data_list[index];
    });
    return 1;
  }
}

export { Form_Helper };
