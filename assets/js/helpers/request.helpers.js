const BASE_URL = "https://62db6cc9e56f6d82a772871d.mockapi.io/BC04/";

const axios_instance = axios.create({
  baseURL: `${BASE_URL}`,
});

const get_all_students = () => {
  return axios_instance.get(`/sv`);
};

const get_one_student_by_id = () => {
  return axios_instance.get(`/sv/id`);
};

const create_new_student = (data) => {
  return axios_instance.post(`/sv`, data);
};

const del_student_by_id = (id) => {
  return axios_instance.delete(`/sv/${id}`);
};

const update_student_by_id = (id, data) => {
  return axios_instance.put(`/sv/${id}`, data);
};

export {
  get_all_students,
  create_new_student,
  del_student_by_id,
  update_student_by_id,
};
