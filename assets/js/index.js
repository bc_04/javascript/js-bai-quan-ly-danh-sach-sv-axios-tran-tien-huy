import {
  add_student,
  delete_student,
  edit_student,
  update_student,
  search_student_by_name,
  get_student_list,
} from "./controller/student.controller.js";

// disabled id field
document.querySelector("#txtMaSV").setAttribute("disabled", "");

get_student_list();

document.getElementById("addSV").addEventListener("click", () => {
  add_student();
});

document.getElementById("clearTable").addEventListener("click", () => {
  clear_form_data();
});

document.getElementById("btnSearch").addEventListener("click", () => {
  let search_name_val = document.getElementById("txtSearch").value;
  search_student_by_name(search_name_val);
});

document.getElementById("updateTable").addEventListener("click", (e) => {
  let student_idx = e.target.dataset.index;
  if (student_idx === undefined) {
    console.log("Nothing to update");
    return;
  }
  update_student(student_idx);
});
