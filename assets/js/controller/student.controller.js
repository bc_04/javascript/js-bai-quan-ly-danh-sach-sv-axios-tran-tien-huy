import { Student } from "../model/student.model.js";
import { Form_Validation_Controller } from "./form_validation.controller.js";
import { Form_Helper } from "../helpers/form.helpers.js";
import { Render_Helper } from "../helpers/render.helpers.js";
import {
  get_all_students,
  create_new_student,
  del_student_by_id,
  update_student_by_id,
} from "../helpers/request.helpers.js";
console.log(Student);
let student_list = [];
let form_obj = new Form_Helper();
form_obj.register_fields([
  "#txtMaSV",
  "#txtTenSV",
  "#txtEmail",
  "#txtPass",
  "#txtDiemToan",
  "#txtDiemLy",
  "#txtDiemHoa",
]);

let extract_form_data = () => {
  let student_obj = form_obj.get_form_data();
  console.log(student_obj);
  return new Student(
    student_obj._id,
    student_obj._name,
    student_obj._email,
    student_obj._pass,
    student_obj._math,
    student_obj._physics,
    student_obj._chem
  );
};

let set_new_form_data = (student_obj) => {
  form_obj.set_form_data([
    student_obj._id,
    student_obj._name,
    student_obj._email,
    student_obj._pass,
    student_obj._math,
    student_obj._physics,
    student_obj._chem,
  ]);
};

let convert_data_to_obj = (student_list = []) => {
  if (student_list.length) {
    for (let i = 0; i < student_list.length; i++) {
      let current_student = student_list[i];
      student_list[i] = new Student(
        current_student._id,
        current_student._name,
        current_student._email,
        current_student._pass,
        current_student._math,
        current_student._physics,
        current_student._chem
      );
    }
  }
};

let clear_form_data = () => {
  form_obj.set_form_data();
};

let render_html_data = (target_el, data, ...callback) => {
  let render_obj = new Render_Helper();
  let [edit_callback, delete_callback] = callback;
  render_obj.set_target_el(target_el);
  render_obj.set_target_data(data);
  render_obj.render_html({ edit_callback, delete_callback });
};

let find_student_by_id = (id) => {
  return student_list.findIndex((student) => student._id === id);
};

let find_student_by_name = (name) => {
  return student_list.findIndex((student) => student._name === name);
};

let get_student_list = (request_callback = get_all_students) => {
  request_callback()
    .then((res) => {
      if (res.data.length) {
        student_list = res.data;
        convert_data_to_obj(student_list);
        render_html_data(
          "#tbodySinhVien",
          student_list,
          edit_student,
          delete_student
        );
      }
    })
    .catch((err) => {
      console.log(err);
    });
};

let add_student = () => {
  let { _id: old_id, ...student_obj } = extract_form_data();
  let form_validation_obj = new Form_Validation_Controller(
    student_obj,
    student_list
  );
  if (form_validation_obj.handle_validation()) {
    create_new_student(student_obj)
      .then((res) => {
        clear_form_data();
        student_list.push(
          new Student(
            res.data._id,
            res.data._name,
            res.data._email,
            res.data._pass,
            res.data._math,
            res.data._physics,
            res.data._chem
          )
        );
        render_html_data(
          "#tbodySinhVien",
          student_list,
          edit_student,
          delete_student
        );
      })
      .catch((err) => {
        console.log("Error !! Can't get data from api.");
      });
  }
};

let delete_student = (id) => {
  del_student_by_id(id)
    .then((res) => {
      console.log(`Delete student with id: ${id} success`);
      get_student_list();
    })
    .catch((err) => {});
};

let edit_student = (id) => {
  console.log(find_student_by_id(id));
  let index = find_student_by_id(id);
  if (index > -1) {
    set_new_form_data(student_list[index]);
  }
  document.getElementById("txtMaSV").setAttribute("disabled", "");
  document.getElementById("addSV").setAttribute("disabled", "");
  document.getElementById("updateTable").removeAttribute("data-index");
  document.getElementById("updateTable").setAttribute("data-index", id);
};

let update_student = (index) => {
  let { _id, ...new_student_obj } = extract_form_data();
  let form_validation_obj = new Form_Validation_Controller(
    new_student_obj,
    student_list
  );
  if (form_validation_obj.handle_validation()) {
    document.getElementById("addSV").removeAttribute("disabled");
    document.getElementById("updateTable").removeAttribute("data-index");
    clear_form_data();
    update_student_by_id(_id, new_student_obj)
      .then((res) => {
        get_student_list();
      })
      .catch((err) => {
        console.log("Error !! Fail to update data");
      });
  }
};

let search_student_by_name = (name) => {
  let find_index = find_student_by_name(name);
  if (find_index > -1) {
    searched_student = student_list[find_index];
    render_html_data("#tbodySinhVien", [searched_student]);
  } else {
    render_html_data("#tbodySinhVien");
  }
};

export {
  add_student,
  delete_student,
  edit_student,
  update_student,
  search_student_by_name,
  get_student_list,
};
