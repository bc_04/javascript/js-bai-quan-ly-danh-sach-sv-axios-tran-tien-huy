import { Validator_Helper } from "../helpers/validator.helper.js";
class Form_Validation_Controller {
  constructor(obj, data_list) {
    this.obj = obj;
    this.data_list = data_list;
    this.check_duplicate = true;
  }

  handle_validation_id(_target) {
    let id_validator = new Validator_Helper(this.obj._id, _target);
    let validation_id = id_validator.is_empty(
      "Mã sinh viên không được phép để rỗng"
    );
    if (this.check_duplicate && this.data_list.length) {
      return (
        validation_id &&
        id_validator.is_exist("Mã sinh viên bị trùng", this.data_list, "_id")
      );
    }
    return validation_id;
  }

  handle_validation_name(_target) {
    return new Validator_Helper(this.obj._name, _target).is_empty(
      "Tên sinh viên không được phép để rỗng"
    );
  }

  handle_validation_email(_target) {
    let email_validadtor = new Validator_Helper(
      this.obj._email.toLowerCase(),
      _target
    );
    let min = 2,
      max = 30;
    return (
      email_validadtor.is_empty("Email không được phép để rỗng") &&
      email_validadtor.is_smaller(
        `Email phải được nhập tối thiểu ${min} ký tự`,
        min
      ) &&
      email_validadtor.is_greater(
        `Email không được phép vượt quá ${max} ký tự`,
        max
      ) &&
      email_validadtor.is_valid_email("Email không hợp lệ")
    );
  }

  handle_validation_pass(_target) {
    let pass_validator = new Validator_Helper(this.obj._pass, _target);
    return pass_validator.is_empty("Mật khẩu không được phép để rỗng");
  }

  handle_validation_math(_target) {
    let math_input_validator = new Validator_Helper(this.obj._math, _target);
    return math_input_validator.is_empty("Điểm toán không được phép để rỗng");
  }

  handle_validation_physics(_target) {
    let physics_input_validator = new Validator_Helper(
      this.obj._physics,
      _target
    );
    return physics_input_validator.is_empty("Điểm lý không được phép để rỗng");
  }

  handle_validation_chemistry(_target) {
    let chemistry_input_validator = new Validator_Helper(
      this.obj._chem,
      _target
    );
    return chemistry_input_validator.is_empty(
      "Điểm hóa không được phép để rỗng"
    );
  }

  handle_validation() {
    let isValid =
      // handle_validation_id("#spanMaSV") &
      this.handle_validation_name("#spanTenSV") &
      this.handle_validation_email("#spanEmailSV") &
      this.handle_validation_pass("#spanMatKhau") &
      this.handle_validation_math("#spanToan") &
      this.handle_validation_physics("#spanLy") &
      this.handle_validation_chemistry("#spanHoa");

    return isValid;
  }
}

export { Form_Validation_Controller };
